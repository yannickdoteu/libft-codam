/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_strlcpy.c                                       :+:    :+:            */
/*                                                     +:+                    */
/*   By: jajaja <jajaja@student.codam.nl>             +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/11/02 14:32:35 by jajaja        #+#    #+#                 */
/*   Updated: 2021/04/12 18:26:54 by jajaja        ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t	ft_strlcpy(char *dst, const char *src, size_t size)
{
	size_t		i;
	int			src_len;

	src_len = ft_strlen(src);
	i = 0;
	if (size <= 0)
		return (src_len);
	while (src[i] != '\0' && i < (size - 1))
	{
		dst[i] = src[i];
		i++;
	}
	dst[i] = '\0';
	return (src_len);
}
