/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_strchr.c                                        :+:    :+:            */
/*                                                     +:+                    */
/*   By: jajaja <jajaja@student.codam.nl>             +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/11/03 17:06:07 by jajaja        #+#    #+#                 */
/*   Updated: 2021/04/20 11:48:19 by jajaja        ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <stddef.h>

char	*ft_strchr(const char *s, int c)
{
	int	i;

	i = 0;
	while (*(s + i) != c)
	{
		if (*(s + i) == '\0')
			return (NULL);
		i++;
	}
	return ((char *)s + i);
}
