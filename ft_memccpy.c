/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_memccpy.c                                       :+:    :+:            */
/*                                                     +:+                    */
/*   By: jajaja <jajaja@student.codam.nl>             +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/01/19 12:39:15 by jajaja        #+#    #+#                 */
/*   Updated: 2021/04/20 11:40:12 by jajaja        ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <stddef.h>

void	*ft_memccpy(void *dest, const void *src, int c, size_t n)
{
	unsigned char	*dst;
	unsigned char	*sorc;
	size_t			i;

	i = 0;
	dst = (unsigned char *)dest;
	sorc = (unsigned char *)src;
	if (n == 0)
		return (NULL);
	while (i < n - 1 && *(sorc + i) != (unsigned char)c)
	{
		*(dst + i) = *(sorc + i);
		i++;
	}
	*(dst + i) = *(sorc + i);
	if (*(sorc + i) == (unsigned char)c)
		return (dst + i + 1);
	return (NULL);
}
