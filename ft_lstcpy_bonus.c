#include "libft.h"

t_list	*ft_lstcpy(t_list *lst, void (*del)(void *))
{
	t_list	*res;
	t_list	*cpy;

	if (lst == NULL)
		return (NULL);
	res = ft_lstnew(lst->content);
	if (res == NULL)
		return (NULL);
	cpy = res;
	lst = lst->next;
	while (lst)
	{
		cpy->next = ft_lstnew(lst->content);
		if (cpy->next == NULL)
		{
			ft_lstclear(&res, del);
			return (NULL);
		}
		cpy = cpy->next;
		lst = lst->next;
	}
	cpy->next = NULL;
	return (res);
}
