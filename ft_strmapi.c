/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_strmapi.c                                       :+:    :+:            */
/*                                                     +:+                    */
/*   By: jajaja <jajaja@student.codam.nl>             +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/01/18 18:39:59 by jajaja        #+#    #+#                 */
/*   Updated: 2021/04/12 18:39:52 by jajaja        ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strmapi(char const *s, char (*f)(unsigned int, char))
{
	int		i;
	int		slen;
	char	*res;

	if (s == NULL || f == NULL)
		return (NULL);
	slen = ft_strlen(s);
	i = 0;
	res = ft_calloc(slen + 1, sizeof(char));
	if (res == NULL)
		return (NULL);
	while (i < slen)
	{
		*(res + i) = f(i, *(s + i));
		i++;
	}
	return (res);
}
