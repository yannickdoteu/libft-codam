/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_lstlast.c                                       :+:    :+:            */
/*                                                     +:+                    */
/*   By: yretel-h <yretel-h@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/03/25 15:34:21 by yretel-h      #+#    #+#                 */
/*   Updated: 2021/04/10 14:59:48 by jajaja        ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_list	*ft_lstlast(t_list *lst)
{
	if (!lst)
		return (lst);
	while (lst->next)
	{
		lst = lst->next;
	}
	return (lst);
}
