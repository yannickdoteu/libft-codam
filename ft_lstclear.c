/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_lstclear.c                                      :+:    :+:            */
/*                                                     +:+                    */
/*   By: yretel-h <yretel-h@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/03/25 16:03:42 by yretel-h      #+#    #+#                 */
/*   Updated: 2021/04/27 12:11:00 by jajaja        ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_lstclear(t_list **lst, void (*del)(void *))
{
	t_list	*next;

	if (*lst == NULL)
		return ;
	while (*lst)
	{
		next = (*lst)->next;
		del((*lst)->content);
		free(*lst);
		*lst = next;
	}
}
