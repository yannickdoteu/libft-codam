/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_atoi.c                                          :+:    :+:            */
/*                                                     +:+                    */
/*   By: jajaja <jajaja@student.codam.nl>             +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/11/10 18:54:31 by jajaja        #+#    #+#                 */
/*   Updated: 2021/04/11 18:52:48 by jajaja        ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int	ft_check(char c)
{
	int	res;

	res = 0;
	if (c == '\n' || c == '\t' || c == ' ')
		res = 1;
	if (c == '\f' || c == '\v' || c == '\r')
		res = 1;
	return (res);
}

int	ft_atoi(const char *nptr)
{
	int	i;
	int	result;
	int	posneg;

	i = 0;
	result = 0;
	posneg = 1;
	while (ft_check(nptr[i]))
	{
		i++;
	}
	if (nptr[i] == '-')
		posneg = -1;
	if (nptr[i] == '-' || nptr[i] == '+')
		i++;
	while (nptr[i] != '\0' && ft_isdigit(nptr[i]) == 1 && (nptr[i]))
	{
		result = result * 10 + (nptr[i] - '0');
		i++;
	}
	return (result * posneg);
}
