/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_strlcat.c                                       :+:    :+:            */
/*                                                     +:+                    */
/*   By: jajaja <jajaja@student.codam.nl>             +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/11/02 14:32:35 by jajaja        #+#    #+#                 */
/*   Updated: 2021/04/08 16:41:24 by yretel-h      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t	ft_strlcat(char *dst, const char *src, size_t size)
{
	int	i;
	int	ii;

	i = 0;
	ii = 0;
	while (*(dst + i) && i < (int)size)
		i++;
	while (*(src + ii) && i < (int)size - 1)
	{
		*(dst + i) = *(src + ii);
		i++;
		ii++;
	}
	if (i < (int)size)
		*(dst + i) = '\0';
	return (i - ii + (int)ft_strlen(src));
}
