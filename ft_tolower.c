/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_tolower.c                                       :+:    :+:            */
/*                                                     +:+                    */
/*   By: yretel-h <yretel-h@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/10/31 15:26:44 by yretel-h      #+#    #+#                 */
/*   Updated: 2021/04/11 18:59:31 by jajaja        ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

static int	ft_isupper(int c)
{
	if (c <= 'Z' && c >= 'A')
		return (1);
	else
		return (0);
}

int	ft_tolower(int c)
{
	if (ft_isupper(c) == 1)
		c = c + 32;
	return (c);
}
