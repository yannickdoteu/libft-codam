/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_memcpy.c                                        :+:    :+:            */
/*                                                     +:+                    */
/*   By: jajaja <jajaja@student.codam.nl>             +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/11/13 12:49:56 by jajaja        #+#    #+#                 */
/*   Updated: 2021/04/20 11:40:39 by jajaja        ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <stddef.h>

void	*ft_memcpy(void *dest, const void *src, size_t n)
{
	char	*ptrd;
	char	*ptrs;
	size_t	i;

	if (dest == NULL && src == NULL)
		return (dest);
	ptrs = (char *)src;
	ptrd = (char *)dest;
	i = 0;
	while (i < n)
	{
		*(ptrd + i) = *(ptrs + i);
		i++;
	}
	return (ptrd);
}
