/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_putnbr_fd.c                                     :+:    :+:            */
/*                                                     +:+                    */
/*   By: jajaja <jajaja@student.codam.nl>             +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/01/17 15:37:06 by jajaja        #+#    #+#                 */
/*   Updated: 2021/04/10 15:00:45 by jajaja        ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int	*ft_min2147483648toa2(char *str)
{
	ft_strlcpy(str, "-2147483648", 12);
	return (0);
}

static int	ft_getsize2(int n, int *neg, int size)
{
	if (n < 0)
	{
		n = n * -1;
		*neg = 1;
		size++;
	}
	while (n >= 10)
	{
		n = n / 10;
		size++;
	}
	return (size);
}

static int	*ft_itoa2(int n, char *str)
{
	int		size;
	int		neg;
	int		i;

	if (n == -2147483648LL)
		return (ft_min2147483648toa2(str));
	size = 1;
	neg = 0;
	size = ft_getsize2(n, &neg, size);
	if (neg == 1)
	{
		*str = '-';
		n = n * -1;
	}
	i = size - 1;
	while (i >= neg)
	{
		*(str + i) = (n % 10) + '0';
		n = n / 10;
		i--;
	}
	return (0);
}

void	ft_putnbr_fd(int n, int fd)
{
	char	str[12];

	ft_bzero(str, 12);
	ft_itoa2(n, str);
	ft_putstr_fd(str, fd);
}
