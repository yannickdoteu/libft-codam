#include "libft.h"

void	ft_lstfree(t_list **lst)
{
	t_list	*next;

	if (*lst == NULL)
		return ;
	while (*lst)
	{
		next = (*lst)->next;
		free(*lst);
		*lst = next;
	}
	return (NULL);
}
