/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_substr.c                                        :+:    :+:            */
/*                                                     +:+                    */
/*   By: jajaja <jajaja@student.codam.nl>             +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/11/18 14:19:48 by jajaja        #+#    #+#                 */
/*   Updated: 2021/04/12 18:33:51 by jajaja        ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_toobigstart(void)
{
	char	*ptr;

	ptr = ft_calloc(1, sizeof(char));
	if (ptr == NULL)
		return (NULL);
	return (ptr);
}

char	*ft_substr(char const *s, unsigned int start, size_t len)
{
	unsigned int	i;
	char			*ptr;
	unsigned int	strlen;

	if (s == NULL)
		return (NULL);
	if (start >= ft_strlen(s))
		return (ft_toobigstart());
	strlen = ft_strlen((s + start));
	if (strlen < len)
		len = strlen;
	ptr = malloc(sizeof(char) * (len + 1));
	if (!ptr)
		return (NULL);
	i = start;
	while (i < len + start)
	{
		*(ptr + i - start) = *(s + i);
		i++;
	}
	*(ptr + i - start) = '\0';
	return (ptr);
}
