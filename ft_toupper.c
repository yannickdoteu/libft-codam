/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_toupper.c                                       :+:    :+:            */
/*                                                     +:+                    */
/*   By: yretel-h <yretel-h@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/10/31 15:26:44 by yretel-h      #+#    #+#                 */
/*   Updated: 2021/04/11 18:59:42 by jajaja        ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

static int	ft_islower(int c)
{
	if (c <= 'z' && c >= 'a')
		return (1);
	else
		return (0);
}

int	ft_toupper(int c)
{
	if (ft_islower(c) == 1)
		(c = c - 32);
	return (c);
}
