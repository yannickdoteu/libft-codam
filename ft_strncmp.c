/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_strncmp.c                                       :+:    :+:            */
/*                                                     +:+                    */
/*   By: jajaja <jajaja@student.codam.nl>             +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/11/17 16:31:04 by jajaja        #+#    #+#                 */
/*   Updated: 2021/04/20 11:46:24 by jajaja        ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <stddef.h>

int	ft_strncmp(const char *s1, const char *s2, size_t n)
{
	int		i;

	i = 0;
	if ((int)n == 0)
		return (0);
	while (*(s1 + i) == *(s2 + i) && *(s1 + i) != '\0' && i < (int)n - 1)
		i++;
	return (((unsigned char)*(s1 + i)) - ((unsigned char)*(s2 + i)));
}
