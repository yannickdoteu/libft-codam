#include "libft.h"

/* returns last pointer up to `size`th element in list. */

t_list	*ft_lsti(t_list *lst, unsigned int size)
{
	unsigned int	i;

	if (lst == NULL)
		return (NULL);
	i = 0;
	while (i < size && lst != NULL)
	{
		lst = lst->next;
		i++;
	}
	return (lst);
}
