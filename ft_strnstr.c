/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_strnstr.c                                       :+:    :+:            */
/*                                                     +:+                    */
/*   By: jajaja <jajaja@student.codam.nl>             +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/11/17 17:22:51 by jajaja        #+#    #+#                 */
/*   Updated: 2021/04/20 11:46:30 by jajaja        ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <stddef.h>

char	*ft_strnstr(const char *big, const char *little, size_t len)
{
	size_t		b;
	size_t		l;
	char		*result;

	b = 0;
	l = 0;
	if (*little == '\0')
		return ((char *)big);
	while (b < len && *(big + b) != '\0')
	{
		if (*(big + b) == *(little + l))
			result = ((char *)big + b);
		while (*(big + b) == *(little + l) && b < len)
		{
			if (*(little + l + 1) == '\0')
				return (result);
			b++;
			l++;
		}
		b -= l;
		l = 0;
		b++;
	}
	return (NULL);
}
