/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_isdigit.c                                       :+:    :+:            */
/*                                                     +:+                    */
/*   By: yretel-h <yretel-h@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/10/31 14:22:19 by yretel-h      #+#    #+#                 */
/*   Updated: 2021/04/11 18:52:32 by jajaja        ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

int	ft_isdigit(int c)
{
	if (c >= '0' && c <= '9')
		return (1);
	else
		return (0);
}
